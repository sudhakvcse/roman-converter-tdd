package com.thoughtworks.vapasi;

public class RomanConverter {

    public Integer convertRomanToArabicNumber(String roman) {

        int valueInArabic = 0, currentCharValue = 0, prevCharValue = 0;

        int isValidNumber = isaValidRomanNumber(roman);
        if (isValidNumber == 1) {

            valueInArabic = returnArabicValue(roman.charAt(roman.length() - 1));
            for (int i = roman.length() - 1; i > 0; i--) {

                currentCharValue = returnArabicValue(roman.charAt(i));
                prevCharValue = returnArabicValue(roman.charAt(i - 1));

                if (currentCharValue > prevCharValue)
                    valueInArabic -= prevCharValue;
                else
                    valueInArabic += prevCharValue;


            }
            return valueInArabic;
        } else {
            throw new IllegalArgumentException("Invalid number");
        }
    }

    public int returnArabicValue(char romanNumeral) {
        switch (romanNumeral) {
            case 'I':
                return 1;
            case 'V':
                return 5;
            case 'X':
                return 10;
            case 'C':
                return 100;
            case 'M':
                return 1000;
            default:
                return 0;

        }

    }

    public int isaValidRomanNumber(String romanNumeral) {

        int flagForValidEnty = 1;
        for (int i = 0; i < romanNumeral.length(); i++) {
            if (romanNumeral.charAt(i) == 'I' || romanNumeral.charAt(i) == 'V' || romanNumeral.charAt(i) == 'X' ||
                    romanNumeral.charAt(i) == 'L' || romanNumeral.charAt(i) == 'M' || romanNumeral.charAt(i) == 'C' ||
                    romanNumeral.charAt(i) == 'D') {
                flagForValidEnty = 1;

            } else {
                flagForValidEnty = -1;
                break;
            }
        }
        return flagForValidEnty;
    }
}






